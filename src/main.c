#include <stdio.h>
#include <glib.h>
#include <gtk/gtk.h>

#include "wkp-sample.h"
#include "wkp-window.h"

static gchar *message = "grupo";

static GOptionEntry entries[] = {
        { "greeter", 'g', 0, G_OPTION_ARG_STRING, &message, "greeter", "g" },
        { NULL }
};

int
main (int argc, char **argv)
{
        WkpSample *sample;
        GError *error = NULL;
        GOptionContext *context;
        WkpWindow *window;

        g_type_init ();

        context = g_option_context_new (" - Displays a greeting message");
        g_option_context_add_main_entries (context, entries, NULL);
        if (!g_option_context_parse (context, &argc, &argv, &error)) {
                g_print ("option parsing failed: %s\n", error->message);
                return -1;
        }

        sample = wkp_sample_new ();
        g_object_set (sample, "name", message, NULL);
        wkp_sample_say_hello (sample);
        g_object_unref (sample);

        gtk_init(&argc, &argv);
        window = wkp_window_new ();
        gtk_widget_show_all (window);

        gtk_main ();

        return 0;
}
