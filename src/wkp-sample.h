/*
 * Copyright (C) 2010 Igalia S.L.
 *
 * Contact: mswl-dm-2009@igalia.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 */

#ifndef _WKP_SAMPLE
#define _WKP_SAMPLE

#include <glib-object.h>

G_BEGIN_DECLS

#define WKP_TYPE_SAMPLE wkp_sample_get_type()
#define WKP_SAMPLE(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), WKP_TYPE_SAMPLE, WkpSample))
#define WKP_SAMPLE_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), WKP_TYPE_SAMPLE, WkpSampleClass))
#define WKP_IS_SAMPLE(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), WKP_TYPE_SAMPLE))
#define WKP_IS_SAMPLE_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), WKP_TYPE_SAMPLE))
#define WKP_SAMPLE_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), WKP_TYPE_SAMPLE, WkpSampleClass))

typedef struct _WkpSamplePrivate WkpSamplePrivate;

typedef struct {
        GObject parent;

        /* <private> */
        WkpSamplePrivate *priv;
} WkpSample;

typedef struct {
        GObjectClass parent_class;
} WkpSampleClass;

GType wkp_sample_get_type (void);
WkpSample* wkp_sample_new (void);

void wkp_sample_set_name (WkpSample *self, const gchar *name);
void wkp_sample_say_hello (WkpSample *self);

G_END_DECLS

#endif /* _WKP_SAMPLE */
