#include "wkp-window.h"
#include <gtk/gtk.h>
#include <webkit/webkit.h>
#include <glib-object.h>

#define WKP_WINDOW_GET_PRIVATE(obj) \
        (G_TYPE_INSTANCE_GET_PRIVATE ((obj), WKP_TYPE_WINDOW, WkpWindowPrivate))

G_DEFINE_TYPE (WkpWindow, wkp_window, GTK_TYPE_WINDOW);

struct _WkpWindowPrivate {
        GtkWidget *webview;
        GtkWidget *scrolled_window;
};

static void
wkp_window_init (WkpWindow *window)
{
        window->priv = WKP_WINDOW_GET_PRIVATE (window);

        window->priv->scrolled_window = gtk_scrolled_window_new (NULL, NULL);
        window->priv->webview = webkit_web_view_new ();

        gtk_container_add (GTK_CONTAINER (window->priv->scrolled_window),
                           window->priv->webview);
        gtk_container_add (GTK_CONTAINER (window),
                           window->priv->scrolled_window);
}

static void
wkp_window_class_init (WkpWindowClass *class)
{
        GObjectClass *g_object_class = (GObjectClass *) class;

        g_type_class_add_private (g_object_class, sizeof (WkpWindowPrivate));
}

GtkWidget *
wkp_window_new (void)
{
        WkpWindow *window;
        window = WKP_WINDOW (g_object_new (WKP_TYPE_WINDOW,
                                           "type", GTK_WINDOW_TOPLEVEL,
                                           NULL));
        return GTK_WIDGET (window);
}
