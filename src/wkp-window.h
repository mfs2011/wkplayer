#ifndef __WKP_WINDOW_H__
#define __WKP_WINDOW_H__

#include <glib.h>
#include <glib-object.h>
#include <gtk/gtk.h>

G_BEGIN_DECLS

typedef struct _WkpWindowClass WkpWindowClass;
typedef struct _WkpWindow WkpWindow;
typedef struct _WkpWindowPrivate WkpWindowPrivate;

#define WKP_TYPE_WINDOW (wkp_window_get_type ())
#define WKP_WINDOW(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), WKP_TYPE_WINDOW, WkpWindow))
#define WKP_WINDOW_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), WKP_TYPE_WINDOW, WkpWindowClass))
#define WKP_IS_WINDOW(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), WKP_TYPE_WINDOW))
#define WKP_IS_WINDOW_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), WKP_TYPE_WINDOW))
#define WKP_WINDOW_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLAS ((obj), WKP_TYPE_WINDOW, WkpWindowClass))

struct _WkpWindowClass {
        GtkWindowClass parent_class;
};

struct _WkpWindow {
        GtkWindow win;

        WkpWindowPrivate *priv;
};

GType    wkp_window_get_type (void);

GtkWidget* wkp_window_new (void);

G_END_DECLS

#endif /* __WKP_WINDOW_H__ */
